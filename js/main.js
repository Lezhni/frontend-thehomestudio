$(document).ready(function() {

	$('.main-slider').slick({
		autoPlay: true,
		autoplaySpeed: 3000,
		arrows: false
	});
	 $('.work-slider').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: true,
	  asNavFor: '.thumbs-slider'
	});
	$('.thumbs-slider').slick({
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  asNavFor: '.work-slider',
	  centerMode: true,
	  centerPadding: '0px',
	  focusOnSelect: true
	});
});